var express = require("express");
const app = express.Router();

const {
  getTours,
  getToursId,
  postTours,
  putTours,
  deleteTours,
} = require("../controller/tours");

app.route("/tours").get(getTours).post(postTours).put(putTours);

app.route("/tours/:id").get(getToursId).delete(deleteTours);

module.exports = app;

const express = require("express");

const mongoose = require('mongoose');
const app = express()
const toursRouter = require('./route/tours')


// database connection
const env = process.env.NODE_ENV || 'development';

const configDB = {
  development: "mongodb+srv://admin:admin@cluster0.b8yne.mongodb.net/touring?retryWrites=true&w=majority",
  production: "mongodb+srv://admin:admin@cluster0.b8yne.mongodb.net/touring?retryWrites=true&w=majority"
};

const dbConnection = configDB[env];

mongoose.set('useCreateIndex', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useNewUrlParser', true)
mongoose.set('useUnifiedTopology', true)
mongoose.connect(dbConnection)
  .then(() => console.log('Database connection successful'))
  .catch(err => console.log(`Error: ${err}`))

app.use(express.json());
app.use('/api/v1', toursRouter)

const port = process.env.PORT || 3000;
app.listen(port, ()=> console.log(`Listening to port ${port}`));

const { fileWrite } = require("../helper/fileWrite");
const fs = require("fs");
const tours = JSON.parse(fs.readFileSync("./data/tours.json"));
const toursModel = require("../schema/touring");

exports.getToursId = async (req, res) => {
  const { id } = req.params;

  const response = await toursModel.findById(id);

  res.status(200).json({
    status: "Success!",
    data: response,
  });
};

exports.getTours = async (req, res) => {
  try {
    const tours = await toursModel.find({});
    res.status(200).json({
      message: "Success!",
      data: tours,
    });
  } catch (err) {
    res.status(500).send(err);
  }
};

exports.postTours = async (req, res) => {
  const { name } = req.body;

  if (name === undefined) {
    return res.status(404).end();
  }

  const tours = await toursModel.find({ name: name });

  if (tours.length === 0) {
    const response = await toursModel.create({ name: name });
    res.status(201).json({ status: "Success!", data: response });
  } else {
    return res.status(403).end();
  }
};

exports.putTours = async (req, res) => {
  const { id, name } = req.body;

  if (name === undefined || id === undefined) {
    return res.status(400).end();
  } else {
    await toursModel.updateOne({ _id: id }, { $set: { name: name } });

    return res.status(201).json({
      status: "Success!",
      data: { id, name },
    });
  }
};

exports.deleteTours = async (req, res) => {
  const { id } = req.params;

  if (id === undefined) {
    return res.status(400).end();
  } else {
    try{
      const response = await toursModel.deleteOne({ _id: id})
      
      return res.status(201).json({status: "Success!"})
    } catch(err){
      return res.status(400).send("NO ID FOUND")
    }
  }
};

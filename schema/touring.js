const mongoose = require("mongoose");

const schema = mongoose.Schema;

const touringSchema = new schema(
  {
    name: {
      type: String,
      required: [true, "This field is mandatory"],
    },
  },
  {
    collection: "tours",
  }
);

const touring = mongoose.model("Tours", touringSchema);

module.exports = touring;
